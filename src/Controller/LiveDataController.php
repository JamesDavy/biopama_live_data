<?php
namespace Drupal\biopama_live_data\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_klc_ecoregion module.
 */
class LiveDataController extends ControllerBase {
  public function content() {
    $element = array(
	  '#theme' => 'live_data',
	  '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }
}