(function ($, Drupal) {
  Drupal.behaviors.biopamaLive = {
    attach: function (context, settings) {
      $(window).once().on('load scroll', function () {

	$('.sidenav').sidenav();
	
	setTimeout(function() {
		$('#sidenav_Tr > i').trigger('click');
	}, 1000);

//Initialise map
//-------------------------------------------------------------------------------------------------------------
// https://developer.mozilla.org/en-US/docs/Web/API/Window/devicePixelRatio (if youhave to work wit canvas)
var pixel_ratio = parseInt(window.devicePixelRatio) || 1;
// leaflet max zoom
var max_zoom = 16;
// Width and height of tiles (reduce number of tiles and increase tile size)
var tile_size = 512;
// zoom to italy (lat,lon, zoom)
var map = L.map('map', {
  zoomControl: false
}).setView([20, 13], 3);

var busy_tabs ={ spinner: "pulsar",color:'#ff5722',background:'#111314c4'};

var busy_wdpa ={ color:'#0a6519',background:'#111314c4'};

// calculate today date
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
if(dd<10) {dd = '0'+dd}
if(mm<10) { mm = '0'+mm}
today = yyyy + '-' + mm + '-' + dd;
// calculate yesterday
var date_1 = new Date();
date_1.setDate(date_1.getDate() - 1);
var dd_1 = date_1.getDate();
var mm_1 = date_1.getMonth()+1; //January is 0!
if(dd_1<10) { dd_1 = '0'+dd_1}
if(mm_1<10) { mm_1 = '0'+mm_1}
var yesterday = date_1.getFullYear()+'-'+ mm_1 +'-'+dd_1;

// calculate initial date 7 days before today
var date_7 = new Date();
date_7.setDate(date_7.getDate() - 7);
var dd_7 = date_7.getDate();
var mm_7 = date_7.getMonth()+1; //January is 0!
if(dd_7<10) { dd_7 = '0'+dd_7}
if(mm_7<10) { mm_7 = '0'+mm_7}
var StartDate_7 = date_7.getFullYear()+'-'+ mm_7 +'-'+dd_7;

// calculate initial date 30 days before today
var date_30 = new Date();
date_30.setDate(date_30.getDate() - 30);
var dd_30 = date_30.getDate();
var mm_30 = date_30.getMonth()+1; //January is 0!
if(dd_30 < 10) {dd_30 = '0'+dd_30}
if(mm_30 <10) {mm_30 = '0'+mm_30}
var StartDate_30 = date_30.getFullYear()+'-'+ mm_30 +'-'+dd_30;

// calculate initial date 90 days before today
var date_90 = new Date();
date_90.setDate(date_90.getDate() - 90);
var dd_90 = date_90.getDate();
var mm_90 = date_90.getMonth()+1; //January is 0!
if(dd_90 < 10) {dd_90 = '0'+dd_90}
if(mm_90 <10) {mm_90 = '0'+mm_90}
var StartDate_90 = date_90.getFullYear()+'-'+ mm_90 +'-'+dd_90;

var fires_time_1 = yesterday+'/'+today
var fires_time_7 = StartDate_7+'/'+today
var fires_time_30 = StartDate_30+'/'+today
var fires_time_90 = StartDate_90+'/'+today




// 	FIRES
var fires_url = 'https://ies-ows.jrc.ec.europa.eu/gwis';
var fires_24 = L.tileLayer.wms(fires_url,  {
   time: fires_time_90,
    layers: 'modis.hs',
    format: 'image/png',
    zIndex: 72,
    transparent: true,
    opacity: 1
});
fires_24.on("load",function() {$(".card-image_fire").busyLoad("hide", {animation: "fade"});});

$('.add_fires').click(function(event) {
  if (map.hasLayer(fires_24)) {
    map.removeLayer(fires_24);
    $('#fire_date_div').empty();
    $('.fires_1d').css('opacity', '0.3');
    $('.fires_7d').css('opacity', '0.3');
    $('.fires_30d').css('opacity', '0.3');
    $('.fires_90d').css('opacity', '0.3');
    $( ".fires_main" ).find( "#show_pa" ).hide();
    $(".card-image_fire").busyLoad("hide", {animation: "fade"});
    $(this).css("color", "#ffffff");
   
}else{
  fires_24.setParams({time:fires_time_1}).addTo(map);
  $('#fire_date_div').empty();
  $('#fire_date_div').html('<div id="info_fires_legeng"><p>Active fires from <b>'+yesterday+'</b> to <b>'+today+'</b></p></div>');
  $('.fires_1d').css('opacity', '1');
  $('.fires_7d').css('opacity', '0.3');
  $('.fires_30d').css('opacity', '0.3');
  $('.fires_90d').css('opacity', '0.3');
  $(".card-image_fire").busyLoad("show", busy_tabs);
  $( ".fires_main" ).find( "#show_pa" ).show();
  $(this).css("color", "#b2610e");
}
});
$('.fires_1d').click(function(event) {
  fires_24.setParams({time:fires_time_1}).addTo(map);
  $('#fire_date_div').empty();
  $('#fire_date_div').html('<div id="info_fires_legeng"><p>Active fires from <b>'+yesterday+'</b> to <b>'+today+'</b></p></div>');
  $('.fires_1d').css('opacity', '1');
  $('.fires_7d').css('opacity', '0.3');
  $('.fires_30d').css('opacity', '0.3');
  $('.fires_90d').css('opacity', '0.3');
  $(".card-image_fire").busyLoad("show", busy_tabs);
});
$('.fires_7d').click(function(event) {
  fires_24.setParams({time:fires_time_7}).addTo(map);
  $('#fire_date_div').empty();
  $('#fire_date_div').html('<div id="info_fires_legeng"><p>Active fires from <b>'+StartDate_7+'</b> to <b>'+today+'</b></p></div>');
  $('.fires_1d').css('opacity', '1');
  $('.fires_7d').css('opacity', '1');
  $('.fires_30d').css('opacity', '0.3');
  $('.fires_90d').css('opacity', '0.3');
  $(".card-image_fire").busyLoad("show", busy_tabs);
});
$('.fires_30d').click(function(event) {
  fires_24.setParams({time:fires_time_30}).addTo(map);
  $('#fire_date_div').empty();
  $('#fire_date_div').html('<div id="info_fires_legeng"><p>Active fires from <b>'+StartDate_30+'</b> to <b>'+today+'</b></p></div>');
  $('.fires_1d').css('opacity', '1');
  $('.fires_7d').css('opacity', '1');
  $('.fires_30d').css('opacity', '1');
  $('.fires_90d').css('opacity', '0.3');
  $(".card-image_fire").busyLoad("show", busy_tabs);
});

$('.fires_90d').click(function(event) {
  fires_24.setParams({time:fires_time_90}).addTo(map);
  $('#fire_date_div').empty();
  $('#fire_date_div').html('<div id="info_fires_legeng"><p>Active fires from <b>'+StartDate_90+'</b> to <b>'+today+'</b></p></div>');
  $('.fires_1d').css('opacity', '1');
  $('.fires_7d').css('opacity', '1');
  $('.fires_30d').css('opacity', '1');
  $('.fires_90d').css('opacity', '1');
  $(".card-image_fire").busyLoad("show", busy_tabs);
});




// floods

var today_floods = new Date();
var dd_floods = today_floods.getDate();
var mm_floods = today_floods.getMonth()+1;
var yyyy_floods = today_floods.getFullYear();
if(mm_floods<10) {mm_floods = '0'+mm_floods}
today_floods = yyyy_floods + '-' + mm_floods + '-' + dd_floods;

var floods_ne_url = 'http://globalfloods-ows.ecmwf.int/glofas-ows/ows.py';
var floods_ne = L.tileLayer.wms(floods_ne_url,  {
    format: 'image/png',
    time:today_floods,
    version: '1.3.0',
    layers:'FloodHazard100y',
   crs: L.CRS.EPSG4326,
    zIndex: 33,
    transparent: true,
    opacity: 1
});
floods_ne.on("load",function() {
  $(".card-image_floods").busyLoad("hide", {animation: "fade"});
  });

  $('.add_floods').click(function(event) {
    if (map.hasLayer(floods_ne)) {
      map.removeLayer(floods_ne);
      $('#floods_date_div').empty();
      $( ".floods_main" ).find( "#show_pa" ).hide();
      $(".card-image_floods").busyLoad("hide", {animation: "fade"});
      $(this).css("color", "#ffffff");
  }else{
    floods_ne.addTo(map);
    $( ".floods_main" ).find( "#show_pa" ).show();
    $(".card-image_floods").busyLoad("show", busy_tabs);
    $('#floods_date_div').html('<div id="info_fires_legeng"><p>Flood hazard 100 year return period - <b>'+today+'</b></p></div>');
    $(this).css("color", "#b2610e");
  }
  });

  // Drought

  var today_edo = new Date();
  var dd_edo = today_edo.getDate();
  var mm_edo = today_edo.getMonth(); //January is 0!
  var yyyy_edo = today_edo.getFullYear();
  if(dd_edo <=10) {dd_edo = '01'}else if(dd_edo >=11 && dd_edo <= 20){dd_edo = '11'}else if(dd_edo >=21 ){dd_edo = '21'}else{}
  if(mm_edo<10) {mm_edo = '0'+mm_edo}
  if(mm_edo==0) {mm_edo = 12}
  today_edo = yyyy_edo + '-' + mm_edo + '-' + dd_edo;
  var mm_spi=mm_edo-1
  var mm_edo=mm_edo-1
  today_spi = yyyy_edo + '-' + '0'+mm_spi;
  var edo_url = 'http://edo.jrc.ec.europa.eu/gdo/php/wms.php';
  var edo = L.tileLayer.wms(edo_url,  {
      layers:'RDrI-Agri',
      SRS:'EPSG:4326',
      format: 'image%2Fpng',
      TIME:today_edo,
      zIndex: 33,
      transparent: true,
      opacity: 0.5
  });
  edo.options.crs = L.CRS.EPSG4326;

  edo.on("load",function() {$("#card-image_drought").busyLoad("hide", {animation: "fade"});});
  $('.add_drought').click(function(event) {
    if (map.hasLayer(edo)) {
      map.removeLayer(edo);
      $('#drought_date_div').empty();
      $( ".drought_main" ).find( "#show_pa" ).hide();
      $(".card-image_drought").busyLoad("hide", {animation: "fade"});
      $(this).css("color", "#ffffff");

  }else{
    edo.addTo(map);
    $( ".drought_main" ).find( "#show_pa" ).show();
    $(".card-image_drought").busyLoad("show", busy_tabs);
    $('#drought_date_div').html('<div id="info_fires_legeng"><p>Updated on <b>'+today_edo+'</b></p></div>');
    $(this).css("color", "#b2610e");
  }
  });




  var spi_url = 'http://edo.jrc.ec.europa.eu/gdo/php/wms.php';
  var spi = L.tileLayer.wms(spi_url,  {
    layers:'spi',
    SRS:'EPSG:4326',
    format: 'image%2Fpng',
    TIME:today_spi,
    zIndex: 33,
    transparent: true,
    opacity: 0.5
  });
  spi.options.crs = L.CRS.EPSG4326;
  spi.on("load",function() {$("#card-image_spi").busyLoad("hide", {animation: "fade"});});

  $('.add_spi').click(function(event) {
    if (map.hasLayer(spi)) {
      map.removeLayer(spi);
      $('#spi_date_div').empty();
      $( ".spi_main" ).find( "#show_pa" ).hide();
      $(".card-image_spi").busyLoad("hide", {animation: "fade"});
      $(this).css("color", "#ffffff");
  }else{
    spi.addTo(map);
    $( ".spi_main" ).find( "#show_pa" ).show();
    $(".card-image_spi").busyLoad("show", busy_tabs);
    $('#spi_date_div').html('<div id="info_fires_legeng"><p>Updated on <b>'+today_spi+'</b></p></div>');
    $(this).css("color", "#b2610e");
  }
  });


//---------------------------------------------------------------------------------------------------------------------------------
// Marine real time layers
//---------------------------------------------------------------------------------------------------------------------------------
//  Daily Sea Surface Temperature Anomaly
//The NOAA Coral Reef Watch (CRW) twice-weekly 50-km Sea Surface Temperature (SST) Anomaly product displays the difference between today's SST and the long-term average. The scale goes from -5 to +5 °C. Positive numbers mean the temperature is warmer than average; negative means cooler than average.
var ssta = L.tileLayer('https://coralreefwatch.noaa.gov/data1/vs/google_maps/ssta/ssta_gm_tiles/{z}/{x}/{y}.png', {
  opacity: 0.5,
	zIndex: 33,
  tms: true
});
ssta.on("load",function() {$(".card-image_ssta").busyLoad("hide", {animation: "fade"});});
$('.add_ssta').click(function(event) {
  if (map.hasLayer(ssta)) {
    map.removeLayer(ssta);
    $('#ssta_date_div').empty();
    $( ".ssta_main" ).find( "#show_pa_marine" ).hide();
    $(".card-image_baa").busyLoad("hide", {animation: "fade"});
    $(this).css("color", "#ffffff");
}else{
  ssta.addTo(map);
  $( ".ssta_main" ).find( "#show_pa_marine" ).show();
  $(".card-image_ssta").busyLoad("show", busy_tabs);
  $('#ssta_date_div').html('<div id="info_fires_legeng"><p>Updated on <b>'+today+'</b></p></div>');
  $(this).css("color", "#b2610e");
}
});
// Daily Coral Bleaching Heat Stress Alert Area
// Level of stress of the Global Coral Reefs derived from NOAA Alerts Bleaching Alerts. If you want to be warned when the heat stress rises to dangerous levels please subscribe here (https://coralreefwatch-satops.noaa.gov/)
var baa = L.tileLayer('https://coralreefwatch.noaa.gov/data1/vs/google_maps/baa_max_running/baa_gm_tiles/{z}/{x}/{y}.png', {
  opacity: 0.5,
	zIndex: 33,
  tms: true
});
baa.on("load",function() {$(".card-image_baa").busyLoad("hide", {animation: "fade"});});
$('.add_baa').click(function(event) {
  if (map.hasLayer(baa)) {
    map.removeLayer(baa);
    $('#baa_date_div').empty();
    $( ".baa_main" ).find( "#show_pa_marine" ).hide();
    $(".card-image_baa").busyLoad("hide", {animation: "fade"});
    $(this).css("color", "#ffffff");
}else{
  baa.addTo(map);
  $( ".baa_main" ).find( "#show_pa_marine" ).show();
  $(".card-image_baa").busyLoad("show", busy_tabs);
  $('#baa_date_div').html('<div id="info_fires_legeng"><p>Updated on <b>'+today+'</b></p></div>');
  $(this).css("color", "#b2610e");
}
});
//  Coral Reef Degree Heating Weeks (3 months period)
//The DHW shows how much heat stress has accumulated in an area over the past 12 weeks (3 months). In other words, we add up the Coral Bleaching HotSpot values whenever the temperature exceeds the bleaching threshold. It is a running sum: as we advance one half-week in time (a timestep), a half-week timestep "falls off" the back end of the 12-week window.
var dhw = L.tileLayer('https://coralreefwatch.noaa.gov/data1/vs/google_maps/dhw/dhw_gm_tiles/{z}/{x}/{y}.png', {
  opacity: 0.5,
	zIndex: 33,
  tms: true
});
dhw.on("load",function() {$(".card-image_dhw").busyLoad("hide", {animation: "fade"});});
$('.add_dhw').click(function(event) {
  if (map.hasLayer(dhw)) {
    map.removeLayer(dhw);
    $('#dhw_date_div').empty();
    $( ".dhw_main" ).find( "#show_pa_marine" ).hide();
    $(".card-image_dhw").busyLoad("hide", {animation: "fade"});
    $(this).css("color", "#ffffff");
}else{
  dhw.addTo(map);
  $( ".dhw_main" ).find( "#show_pa_marine" ).show();
  $(".card-image_dhw").busyLoad("show", busy_tabs);
  $('#dhw_date_div').html('<div id="info_fires_legeng"><p>Updated on <b>'+today+'</b></p></div>');
  $(this).css("color", "#b2610e");
}
});

//  Coral Bleaching HotSpot (HS)
var hs = L.tileLayer('https://coralreefwatch.noaa.gov/data1/vs/google_maps/hs/hs_gm_tiles/{z}/{x}/{y}.png', {
  opacity: 0.5,
	zIndex: 33,
  tms: true
});
hs.on("load",function() {$(".card-image_hs").busyLoad("hide", {animation: "fade"});});
$('.add_hs').click(function(event) {
  if (map.hasLayer(hs)) {
    map.removeLayer(hs);
    $('#hs_date_div').empty();
    $( ".hs_main" ).find( "#show_pa_marine" ).hide();
    $(".card-image_hs").busyLoad("hide", {animation: "fade"});
    $(this).css("color", "#ffffff");
}else{
  hs.addTo(map);
  $( ".hs_main" ).find( "#show_pa_marine" ).show();
  $(".card-image_hs").busyLoad("show", busy_tabs);
  $('#hs_date_div').html('<div id="info_fires_legeng"><p>Updated on <b>'+today+'</b></p></div>');
  $(this).css("color", "#b2610e");
}
});
//  Coral Bleaching HotSpot (HS)
var sstat = L.tileLayer('https://coralreefwatch.noaa.gov/data1/vs/google_maps/sst_trend/sst_trend_gm_tiles/{z}/{x}/{y}.png', {
  opacity: 0.5,
	zIndex: 33,
  tms: true
});
sstat.on("load",function() {$(".card-image_sstat").busyLoad("hide", {animation: "fade"});});
$('.add_sstat').click(function(event) {
  if (map.hasLayer(sstat)) {
    map.removeLayer(sstat);
    $('#sstat_date_div').empty();
    $( ".sstat_main" ).find( "#show_pa_marine" ).hide();
    $(".card-image_baa").busyLoad("hide", {animation: "fade"});
    $(this).css("color", "#ffffff");
}else{
  sstat.addTo(map);
  $( ".sstat_main" ).find( "#show_pa_marine" ).show();
  $(".card-image_sstat").busyLoad("show", busy_tabs);
  $('#sstat_date_div').html('<div id="info_fires_legeng"><p>Updated on <b>'+today+'</b></p></div>');
  $(this).css("color", "#b2610e");
}
});




// Define basemaps
// choose one from https://leaflet-extras.github.io/leaflet-providers/preview/
var WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
 attribution: ''
}).addTo(map);

var light  =  L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
  subdomains: 'abcd',
  opacity: 0.9,
	maxZoom: 19
}).addTo(map);


// Lable pane (no additional library required)
var topPane = map.createPane('leaflet-top-pane', map.getPanes().mapPane);
var topLayer =  L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_only_labels/{z}/{x}/{y}.png', {
	subdomains: 'abcd',
  opacity: 1,
	maxZoom: 19
}).addTo(map);
topPane.appendChild(topLayer.getContainer());
topLayer.setZIndex(2);



// WDPA WMS GEOSERVER LAYER - SETUP
var url = 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms';
var wdpa=L.tileLayer.wms(url, {layers: 'dopa_explorer_3:dopa_geoserver_wdpa_master_201905',transparent: true,format: 'image/png',
featureInfoFormat: 'text/javascript',opacity:'0.4', makeLayerQueryable: true,zIndex: 33});
wdpa.on("load",function() {$(".nav-wrapper").busyLoad("hide", {animation: "fade"});});
// WDPA filter
wdpa.setParams({CQL_FILTER:"marine <> 2"});

var wdpa_marine=L.tileLayer.wms(url, {layers: 'dopa_explorer_3:dopa_geoserver_wdpa_master_201905',transparent: true,format: 'image/png',
featureInfoFormat: 'text/javascript',opacity:'0.4', makeLayerQueryable: true,zIndex: 33});
wdpa_marine.on("load",function() {$(".nav-wrapper").busyLoad("hide", {animation: "fade"});});
// WDPA filter
wdpa_marine.setParams({CQL_FILTER:"marine = 2"});
$( ".card-content" ).find( "#show_pa" ).click(function(event) {
  if (map.hasLayer(wdpa)) {
    map.removeLayer(wdpa);

}else{
  wdpa.addTo(map);
  $(".nav-wrapper").busyLoad("show", busy_wdpa);
}
});

$( ".card-content" ).find( "#show_pa_marine" ).click(function(event) {
  if (map.hasLayer(wdpa_marine)) {
    map.removeLayer(wdpa_marine);

}else{
  wdpa_marine.addTo(map);
  $(".nav-wrapper").busyLoad("show", busy_wdpa);
}
});


//Available Layers
var baseMaps = {"White" : light, "WorldImagery":WorldImagery};

      });
    }
  };
})(jQuery, Drupal);